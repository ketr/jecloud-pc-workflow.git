import { getRunTasks } from '@/api/monitoring';
/**
 * 获得流程运行的节点
 */
export function initRunNodeData(props, runNodes) {
  // 流程监控模式
  if (props.monitoringOperation) {
    // 调接口找到运行的节点
    getRunTasks({ piid: props.monitoringInfo.dataInfo.piid }).then((data) => {
      const nodeIds = [];
      data.forEach((item) => {
        nodeIds.push(item.taskDefinitionKey);
      });
      runNodes.value = nodeIds;
    });
  }
}
/**
 * 判断节点是否可编辑
 * @param {*} editor
 * @param {*} emit
 * @param {*} props
 * @param {*} nodeData
 * @returns
 */
export function doCanvasNodeEdit(editor, emit, props, nodeData) {
  let flag = true;
  const selectModel = editor.value.$editor.graph.getSelectionModel();
  if (props.monitoringOperation) {
    if (
      (props.runNodes.length > 0 && props.runNodes.includes(nodeData.id)) ||
      selectModel.cells.length > 1
    ) {
      selectModel.clear();
      emit('getActiveKey', { key: 'base', type: 'base' });
      flag = false;
    }
  }
  return flag;
}
/**
 * 封装节点数据便于后续操作
 * @param {*} jebpmData
 */
export function getOriginalNodeData(jebpmData) {
  const originalNodeData = {};
  jebpmData.childShapes.forEach((item) => {
    const { resourceId } = item;
    const nodeType = item.stencil.id;
    if (nodeType != 'line') {
      // 节点配置
      originalNodeData[resourceId] = item;
    }
  });
  // 基础配置
  originalNodeData.baseConfig = jebpmData.properties;
  return originalNodeData;
}
