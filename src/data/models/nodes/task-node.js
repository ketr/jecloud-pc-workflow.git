import BaseNode from '../base-node';
import NodeBaseConfig from '../attrs/node-base-config';
import NodeAssignmentConfig from '../attrs/node-assignment-config';
import NodeRejectConfig from '../attrs/node-reject-config';
import NodeWarningDelays from '../attrs/node-warning-delays';
import NodeCirculateConfig from '../attrs/node-circulate-config';
import NodeEndorseConfig from '../attrs/node-endorse-config';
import NodeApprovaNotice from '../attrs/node-approva-notice';
import NodeFormControl from '../attrs/node-form-control';
export default class TaskNode extends BaseNode {
  constructor(options) {
    super(options);

    /**
     * 基础配置
     */
    this.properties = new NodeBaseConfig(options.properties || {}, this.stencil.id);

    /**
     * 可处理人
     */
    this.assignmentConfig = new NodeAssignmentConfig(options.assignmentConfig || {});

    /**
     * 提交分解
     * [
        {
          "buttonName": "按钮名称",
          "nodeId": "节点id",
          "nodeName": "节点name"
        }
      ]
     */
    this.commitBreakdown = options.commitBreakdown || [];

    /**
     * 驳回配置
     */
    this.dismissConfig = new NodeRejectConfig(options.dismissConfig || {});

    /**
     * 预警延期
     */
    this.earlyWarningAndPostponement = new NodeWarningDelays(
      options.earlyWarningAndPostponement || {},
    );

    /**
     * 传阅配置
     */
    this.passRoundConfig = new NodeCirculateConfig(options.passRoundConfig || {});

    /**
     * 按钮配置
     * [
        {
          "operationId": "taskDelegateOperation",
          "buttonId": "delegateBtn",
          "buttonCode": "delegateBtn",
          "buttonName": "委托",
          "appListeners": null,
          "pcListeners": null,
          "backendListeners": null,
          "displayExpression": null
        }
      ]
     */
    this.buttonsConfig = options.buttonsConfig || [];

    /**
     * 事件配置
     * [
        {
          "type_name(事件类型)": "",
          "type_code(事件类型)": "",
          "executionStrategy_name(执行策略)": "",
          "executionStrategy_code(执行策略)": "",
          "assignmentFieldConfiguration（赋值字段配置）": "",
          "serviceName (业务bean)": "",
          "method(业务方法)": "",
          "existenceParameter(是否存在参数)": true
        }
      ]
     */
    this.customEventListeners = options.customEventListeners || [];

    /**
     * 加签配置
     */
    this.addSignature = new NodeEndorseConfig(options.addSignature || {});

    /**
     * 审批告知
     */
    this.approvalNotice = new NodeApprovaNotice(options.approvalNotice || {});

    /**
     * 表单控制
     */
    this.formConfig = new NodeFormControl(options.formConfig || {});
  }
}
