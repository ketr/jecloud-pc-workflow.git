import { isNotEmpty } from '@jecloud/utils';
export default class NodeRejectConfig {
  constructor(options) {
    /**
     * 可驳回
     */
    this.enable = isNotEmpty(options.enable) ? options.enable : '0';

    /**
     * 驳回节点名称
     */
    this.dismissTaskNames = isNotEmpty(options.dismissTaskNames) ? options.dismissTaskNames : '';

    /**
     * 驳回节点Id
     */
    this.dismissTaskIds = isNotEmpty(options.dismissTaskIds) ? options.dismissTaskIds : [];

    /**
     * 驳回配置列表数据
     * [
          {
            "buttonName": "按钮名称",
            "nodeId": "节点id",
            "nodeName": "节点name"
          }
       ]
     */
    this.commitBreakdown = isNotEmpty(options.commitBreakdown) ? options.commitBreakdown : [];

    /**
     * 可直接提交驳回人
     */
    this.directSendAfterDismiss = isNotEmpty(options.directSendAfterDismiss)
      ? options.directSendAfterDismiss
      : '0';

    /**
     * 被驳回后强制提交
     */
    this.forceCommitAfterDismiss = isNotEmpty(options.forceCommitAfterDismiss)
      ? options.forceCommitAfterDismiss
      : '0';

    /**
     * 被驳回后禁用送交
     */
    this.disableSendAfterDismiss = isNotEmpty(options.disableSendAfterDismiss)
      ? options.disableSendAfterDismiss
      : '0';

    /**
     * 不可退回
     */
    this.noReturn = isNotEmpty(options.noReturn) ? options.noReturn : '0';

    /**
     * 可直接送交退回人
     */
    this.directSendAfterReturn = isNotEmpty(options.directSendAfterReturn)
      ? options.directSendAfterReturn
      : '0';
  }
}
