export default class FormInformRemind {
  constructor(options, id) {
    /**
     * 通知模板
     * {
        flowBacklog: '',
        title: '流程[{@PROCESS_NAME@}]提醒',
        dwr: '流程提醒：由<font color=red>{@USER_NAME@}</font>在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}任务,执行操作：{@SUBMIT_OPERATE@},执行意见：',
        email:
          '你有一条任务需要审批!\n由{@USER_NAME@}在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}流程任务,执行动作：{@SUBMIT_OPERATE@},执行意见：{@SUBMIT_COMMENTS@}',
        note: '流程提醒：由{@USER_NAME@}在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}任务，请尽快处理!',
        thirdparty:
          '你有一条任务需要审批!\n由{@USER_NAME@}在{@NOW_TIME@}给您{@SUBMIT_OPERATE@}了{@PROCESS_NAME@}流程任务,执行动作：{@SUBMIT_OPERATE@},执行意见：{@SUBMIT_COMMENTS@}',
     * }
     * 
     */
    this.messageDefinitions = options.messageDefinitions || {
      flowBacklog: '',
      title: '流程[{@PROCESS_NAME@}]提醒',
      dwr: '流程提醒：由<font color=red>{@USER_NAME@}</font>在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}任务,执行操作：{@SUBMIT_OPERATE@},执行意见：{@SUBMIT_COMMENTS@}',
      email:
        '你有一条任务需要审批!<br>\n由{@USER_NAME@}在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}流程任务<br>\n执行动作：{@SUBMIT_OPERATE@},执行意见：{@SUBMIT_COMMENTS@}<br>\n当前活动：{@PROCESS_CURRENTTASK@}，请尽快审批!<br>',
      note: '流程提醒：由{@USER_NAME@}在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}任务，请尽快处理!',
      canUrged: '',
      dingTalk:
        '<font color="red">{@URGENCY@}</font></br>' +
        '<font color="black">由{@USER_NAME@}在{@NOW_TIME@}给您提交了{@PROCESS_NAME@}任务，请尽快审批！</font></br>' +
        '<font color="black">执行操作：{@SUBMIT_OPERATE@}</font></br>' +
        '<font color="black">执行意见：{@SUBMIT_COMMENTS@}</font></br>' +
        '<font color="black">当前节点：{@PROCESS_CURRENTTASK@}</font></br>',
      weWork:
        '你有一条任务需要审批!<br/>由{@USER_NAME@}在{@NOW_TIME@}给您{@SUBMIT_OPERATE@}了{@PROCESS_NAME@}流程任务,<br/>执行动作：{@SUBMIT_OPERATE@},执行意见：{@SUBMIT_COMMENTS@}<br/>当前活动：{@PROCESS_CURRENTTASK@},请尽快审批!',
    };

    /**
     * 提醒方式
     * 只有新建的时候给默认值
     */
    this.messages = options.messages || id ? options.messages : 'WEB';

    /**
     * 钉钉配置id
     */
    this.jeDingTalkId = options.jeDingTalkId || '';

    /**
     * 钉钉配置名称
     */
    this.jeDingTalkName = options.jeDingTalkName || '';
  }
}
