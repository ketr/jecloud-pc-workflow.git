import { isNotEmpty } from '@jecloud/utils';
export default class NodeCirculateConfig {
  constructor(options) {
    /**
     * 团队名称
     */
    this.teamName = isNotEmpty(options.teamName) ? options.teamName : '团队名称';

    /**
     * 随机派发
     */
    this.distributeRandom = isNotEmpty(options.distributeRandom) ? options.distributeRandom : '1';

    /**
     * 轮询派发
     */
    this.distributedPolling = isNotEmpty(options.distributedPolling)
      ? options.distributedPolling
      : '0';
  }
}
