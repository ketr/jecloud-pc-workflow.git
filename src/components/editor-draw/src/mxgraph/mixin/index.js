import { mxgraph } from '..';

/**
 * 加载语言
 * @param {*} lang
 * @returns
 */
export function loadLocales(lang) {
  const { mxResources } = mxgraph;
  return import(`../locales/${lang}.txt`).then((info) => {
    mxResources.parse(info);
  });
}

export const urlParams = {};
