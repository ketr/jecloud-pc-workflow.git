/**
 * 用于编写api方法
 * api url 统一在urls.js中声明导出使用，与方法分开
 */
import { ajax } from '@jecloud/utils';
import {
  APT_WORKFLOW_MONITORING_GET_STATISTICS,
  API_WORKFLOW_COMMON_GET_INFOBYID,
  API_WORKFLOW_PREVIEW_IMAGE,
  API_WORKFLOW_MONITOR_GET_ALLNODE,
  API_WORKFLOW_MONITOT_ADJUST_RUNNING_NODE,
  API_WORKFLOW_MONITOR_GET_RUN_TASKS,
  API_WORKFLOW_MONITOR_ADJUST_RUNNING_NODE_ASSIGNEE,
  API_WORKFLOW_MONITOR_GET_NODE_ASSIGNEE,
  API_WORKFLOW_MONITOR_GET_INFOBYPIID,
  API_WORKFLOW_MONITOR_UPDATEADNDEPLOY_BYPIID,
  API_WORKFLOW_MONITOR_GET_HISTORY_INFOBY_OLANNINGLOGID,
} from './urls';

/**
 * 获得流程监控展板数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getStatistics() {
  return ajax({
    method: 'GET',
    url: APT_WORKFLOW_MONITORING_GET_STATISTICS,
  }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得流程监控展板数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getInfoById({ params, headers }) {
  return ajax({
    params,
    url: API_WORKFLOW_COMMON_GET_INFOBYID,
    headers,
  }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得流程图
 * @param {*} beanId 功能业务id
 * @param {*} pdId 工作流id
 * @returns
 */
export function getWorkFlowImage(params) {
  // 时间戳，防止缓存
  params._t = new Date().getTime();
  return ajax({
    url: API_WORKFLOW_PREVIEW_IMAGE,
    params,
    method: 'GET',
  }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得前后跳转操作的节点
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getMonitorAllNode(params) {
  return ajax({
    params,
    url: API_WORKFLOW_MONITOR_GET_ALLNODE,
  }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 前后跳转操作
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function adjustRunningNode(params) {
  return ajax({
    params,
    url: API_WORKFLOW_MONITOT_ADJUST_RUNNING_NODE,
  }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得运行中的节点
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getRunTasks(params) {
  return ajax({
    params,
    url: API_WORKFLOW_MONITOR_GET_RUN_TASKS,
  }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 调拨操作
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function adjustRunningNodeAssignee(params) {
  return ajax({
    params,
    url: API_WORKFLOW_MONITOR_ADJUST_RUNNING_NODE_ASSIGNEE,
  }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 获得节点人员
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getNodeAssignee(params) {
  return ajax({
    params,
    url: API_WORKFLOW_MONITOR_GET_NODE_ASSIGNEE,
  }).then((info) => {
    if (info != '') {
      return info.data;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 查询流程规划数据
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getInfoByPiid(params) {
  return ajax({
    url: API_WORKFLOW_MONITOR_GET_INFOBYPIID,
    params: params,
  }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 数据保存并发布接口
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function doUpdateAndDeployByPiid(params) {
  return ajax({
    url: API_WORKFLOW_MONITOR_UPDATEADNDEPLOY_BYPIID,
    params: params,
  }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}

/**
 * 根据流程历史发布id获取流程规划信息
 *
 * @export
 * @param {Object} params
 * @return {Promise}
 */
export function getHistoryInfoByPlanningLogId(params) {
  return ajax({
    url: API_WORKFLOW_MONITOR_GET_HISTORY_INFOBY_OLANNINGLOGID,
    params: params,
  }).then((info) => {
    if (info != '') {
      return info;
    } else {
      return Promise.reject(info);
    }
  });
}
